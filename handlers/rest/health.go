package rest

import (
	"io"
	"net/http"

	"github.com/Shopify/sarama"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
)

const healthTopic = "health"

type healthCtrl struct {
	version, gosuslugiHost string
	admin                  sarama.ClusterAdmin
	prod                   sarama.SyncProducer
}

func newHealthCtrl(
	version, gosuslugiHost string,
	admin sarama.ClusterAdmin,
	prod sarama.SyncProducer,
) *healthCtrl {
	return &healthCtrl{
		version:       version,
		admin:         admin,
		prod:          prod,
		gosuslugiHost: gosuslugiHost,
	}
}

func (hc *healthCtrl) ready(ctx *gin.Context) {
	var isErr bool
	res := struct {
		Version   string `json:"version"`
		Kafka     string `json:"kafka"`
		Gosuslugi string `json:"gosuslugi"`
	}{
		Version:   hc.version,
		Kafka:     "ok",
		Gosuslugi: "ok",
	}

	if err := hc.checkKafka(); err != nil {
		res.Kafka = err.Error()
		isErr = true
	}

	if err := hc.checkGosuslugi(); err != nil {
		res.Gosuslugi = err.Error()
		isErr = true
	}

	if isErr {
		ctx.AbortWithStatusJSON(503, res)
		return
	}
	ctx.JSON(200, res)
}

func (hc *healthCtrl) checkKafka() error {
	if err := hc.admin.CreateTopic(healthTopic, &sarama.TopicDetail{
		NumPartitions:     1,
		ReplicationFactor: 1,
	}, false); err != nil {
		return err
	}

	if _, _, err := hc.prod.SendMessage(&sarama.ProducerMessage{Topic: healthTopic}); err != nil {
		return err
	}

	return hc.admin.DeleteTopic(healthTopic)
}

func (hc *healthCtrl) checkGosuslugi() error {
	resp, err := http.Get(hc.gosuslugiHost + "/ready")
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		b, _ := io.ReadAll(resp.Body)
		return errors.New(string(b))
	}

	return nil
}
