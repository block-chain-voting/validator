package rest

import (
	"net/http"

	"github.com/Shopify/sarama"
	"github.com/gin-gonic/gin"
)

func NewHealthHandler(version, gosuslugiHost string, admin sarama.ClusterAdmin, prod sarama.SyncProducer) http.Handler {
	hc := newHealthCtrl(version, gosuslugiHost, admin, prod)

	mux := gin.Default()
	mux.GET("/ready", hc.ready)

	return mux
}
