IMAGE_REPO = registry.gitlab.com/block-chain-voting/validator
IMAGE_TAG ?= latest

build:
	docker build -t $(IMAGE_REPO):$(IMAGE_TAG) .

push: build
	docker push $(IMAGE_REPO):$(IMAGE_TAG)
