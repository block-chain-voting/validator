package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/Shopify/sarama"
	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
	"github.com/pkg/errors"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/rs/zerolog/pkgerrors"

	"gitlab.com/block-chain-voting/validator/handlers/amqp"
	"gitlab.com/block-chain-voting/validator/handlers/rest"
	"gitlab.com/block-chain-voting/validator/services/votervalidator"
)

var (
	version string
	envs    struct {
		Web struct {
			HealthAddr      string        `envconfig:"WEB_HEALTH_ADDR" default:":4000"`
			ShutdownTimeout time.Duration `envconfig:"WEB_SHUTDOWN_TIMEOUT" default:"5s"`
		}
		Kafka struct {
			Addr  string `envconfig:"KAFKA_ADDR" required:"true"`
			Topic struct {
				IsDead    bool   `envconfig:"KAFKA_TOPIC_IS_DEAD" default:"false"`
				Name      string `envconfig:"KAFKA_TOPIC_NAME" required:"true"`
				ChainName string `envconfig:"KAFKA_TOPIC_CHAIN_NAME" required:"true"`
			}
			Consumer struct {
				GroupID             string        `envconfig:"KAFKA_CONSUMER_GROUP_ID" default:"validator"`
				ConsumptionInterval time.Duration `envconfig:"KAFKA_CONSUMER_CONSUMPTION_INTERVAL" default:"10s"`
			}
		}
		Gosuslugi struct {
			Host string `envconfig:"GOSUSLUGI_HOST" required:"true"`
		}
	}
)

func main() {
	zerolog.ErrorStackMarshaler = pkgerrors.MarshalStack
	log.Logger = zerolog.New(zerolog.ConsoleWriter{Out: os.Stderr}).With().Caller().Logger()

	if err := run(); err != nil {
		log.Fatal().Stack().Err(err).Msg("main: Fatal error")
	}
}

func run() error {
	if err := godotenv.Load(); err != nil && !os.IsNotExist(err) {
		log.Warn().Err(err).Msg("Read ENVs from .env file")
	}
	if err := envconfig.Process("", &envs); err != nil {
		return errors.Wrap(err, "failed to parse ENVs to struct")
	}

	log.Info().Interface("envs", envs).Msg("ENVs")

	validatorService := votervalidator.New(envs.Gosuslugi.Host)

	cfg := sarama.NewConfig()
	cfg.Producer.Return.Errors = true
	cfg.Producer.Return.Successes = true
	cfg.Consumer.Return.Errors = true
	cfg.Consumer.Offsets.Initial = sarama.OffsetOldest

	kafkaClient, err := sarama.NewClient([]string{envs.Kafka.Addr}, cfg)
	if err != nil {
		return errors.Wrap(err, "failed to connect to Kafka broker")
	}

	saramaAdmin, err := sarama.NewClusterAdminFromClient(kafkaClient)
	if err != nil {
		return errors.Wrap(err, "failed to init Kafka admin")
	}

	consumerGroup, err := sarama.NewConsumerGroupFromClient(envs.Kafka.Consumer.GroupID, kafkaClient)
	if err != nil {
		return errors.Wrapf(err, "failed to init Kafka consumer group %q", envs.Kafka.Consumer.GroupID)
	}

	defer func() {
		log.Info().Msgf("Closing Kafka consumer group %q", envs.Kafka.Consumer.GroupID)
		if err := consumerGroup.Close(); err != nil {
			log.Error().Stack().Err(err).Msg("main: Failed to close Kafka consumer group")
		}
	}()

	producer, err := sarama.NewSyncProducerFromClient(kafkaClient)
	if err != nil {
		return errors.Wrap(err, "failed to init Kafka producer")
	}

	defer func() {
		log.Info().Msg("Closing Kafka producer")
		if err := producer.Close(); err != nil {
			log.Error().Stack().Err(err).Msg("main: Failed to close Kafka producer")
		}
	}()

	var options []amqp.ValidateVoterHandlerOption
	if envs.Kafka.Topic.IsDead {
		options = append(options, amqp.WithTopicDead(envs.Kafka.Consumer.ConsumptionInterval))
	}
	validateVoterHandler := amqp.NewValidateVoterHandler(
		envs.Kafka.Topic.ChainName,
		validatorService,
		producer,
		options...,
	)

	healthHandler := rest.NewHealthHandler(version, envs.Gosuslugi.Host, saramaAdmin, producer)
	httpErrs := make(chan error)
	go func() {
		log.Info().Msgf("Listening health service on %s...", envs.Web.HealthAddr)
		if err := http.ListenAndServe(envs.Web.HealthAddr, healthHandler); err != nil && !errors.Is(err, http.ErrServerClosed) {
			httpErrs <- err
		}
	}()

	go func() {
		for err := range consumerGroup.Errors() {
			log.Error().Stack().Err(err).Msg("Consumer error")
		}
	}()

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	go func() {
		format := "Consuming from topic=%s, group ID=%s"
		if envs.Kafka.Topic.IsDead {
			format = "Consuming from dead topic=%s, group ID=%s"
		}
		log.Info().Msgf(format, envs.Kafka.Topic.Name, envs.Kafka.Consumer.GroupID)

		for {
			if err := consumerGroup.Consume(
				ctx,
				[]string{envs.Kafka.Topic.Name},
				validateVoterHandler,
			); err != nil && !errors.Is(err, sarama.ErrClosedConsumerGroup) {
				log.Fatal().Err(err).Msgf("Failed to consume from topic=%s", envs.Kafka.Topic.Name)
			}

			if ctx.Err() != nil {
				return
			}
		}
	}()

	sigint := make(chan os.Signal, 1)
	signal.Notify(sigint, syscall.SIGINT)

	select {
	case err := <-httpErrs:
		return errors.WithStack(err)
	case sig := <-sigint:
		log.Info().Msgf("Received signal: %s", sig)
	}
	return nil
}
